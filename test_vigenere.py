import pytest
from vigenere import vigenere_crypter,vigenere_decrypter

def test_vigenere_cipher():
    assert vigenere_crypter("CA VA ETRE TOUT NOIR", "PIERRE") == "RI ZR VXGM XFLX CWMI"
    assert vigenere_crypter("AAA AAA AAA AAA AAA AAA", "ABCD") == "ABC DAB CDA BCD ABC DAB"
    #Les deux exemples du tp
def test_vigenere_decipher():
    assert vigenere_decrypter("RI ZR VXGM XFLX CWMI", "PIERRE") == "CA VA ETRE TOUT NOIR"
    assert vigenere_decrypter("ABC DAB CDA BCD ABC DAB", "ABCD") == "AAA AAA AAA AAA AAA AAA"
    #Les deux exemples du tp
    
#Pas de résultat concluant :-(
# def test_vigenere_decrypt():
#     assert vigenere_decrypt("RI ZR VXGM XFLX CWMI") == "CA VA ETRE TOUT NOIR"
