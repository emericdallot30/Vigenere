import string

def vigenere_crypter(message, key):
    """
    Cette fonction prend un message et une clé et renvoie le message crypté par le chiffrement de Vigenère.
    Les espaces sont conservés dans le message crypté.
    """
    message = message.upper()
    key = key.upper()
    encrypted_message = ""
    key_index = 0

    for i in range(len(message)):
        if message[i] == " ":
            encrypted_message += " "
        else:
            key_letter = key[key_index % len(key)]
            encrypted_letter = chr(((ord(message[i]) + ord(key_letter) - 2 * ord('A')) % 26) + ord('A'))
            encrypted_message += encrypted_letter
            key_index += 1

    return encrypted_message     # Renvoi du message crypté

def vigenere_decrypter(encrypted_message, key):
    """
    Cette fonction prend un message crypté et une clé et renvoie le message d'origine déchiffré par le chiffrement de Vigenère.
    Les espaces sont conservés dans le message déchiffré.
    """
    encrypted_message = encrypted_message.upper()
    key = key.upper()
    deciphered_message = ""

    key_index = 0

    # Parcours du message crypté caractère par caractère
    for i in range(len(encrypted_message)):
        # Si le caractère courant est un espace, on l'ajoute directement au message déchiffré
        if encrypted_message[i] == " ":
            deciphered_message += " "
        else:
            key_letter = key[key_index % len(key)]
            deciphered_letter = chr(((ord(encrypted_message[i]) - ord(key_letter) + 26) % 26) + ord('A'))
            deciphered_message += deciphered_letter
            key_index += 1

    
    return deciphered_message # Renvoi du message déchiffré
#Cette méthode ne marche pas toujours avec les tests 
def vigenere_decrypt(ciphertext):
    ciphertext = ''.join(filter(str.isalpha, ciphertext.upper()))
    freq_french = {'A': 0.089, 'B': 0.010, 'C': 0.030, 'D': 0.036, 'E': 0.157, 
                   'F': 0.010, 'G': 0.009, 'H': 0.008, 'I': 0.083, 'J': 0.003, 
                   'K': 0.001, 'L': 0.050, 'M': 0.030, 'N': 0.071, 'O': 0.053, 
                   'P': 0.030, 'Q': 0.010, 'R': 0.065, 'S': 0.080, 'T': 0.073, 
                   'U': 0.058, 'V': 0.013, 'W': 0.003, 'X': 0.004, 'Y': 0.030, 
                   'Z': 0.002}
    ic_list = []
    for key_length in range(1, len(ciphertext)):
        ic_sum = 0
        for offset in range(key_length):
            subtext = ciphertext[offset::key_length]
            freq_subtext = {letter: subtext.count(letter) / len(subtext) for letter in string.ascii_uppercase}
            ic_sum += sum(freq * (freq - 1) for freq in freq_subtext.values())
        ic = ic_sum / (key_length * (len(ciphertext) // key_length))
        ic_list.append(ic)

    key_length = ic_list.index(max(ic_list)) + 1

    key = ''
    for i in range(key_length):
        subtext = ''
        for j in range(i, len(ciphertext), key_length):
            subtext += ciphertext[j]
        freq_subtext = {letter: subtext.count(letter) / len(subtext) for letter in string.ascii_uppercase}
        max_corr = 0
        for j in range(26):
            corr = sum(freq_french[letter] * freq_subtext[(chr((ord(letter) - j - 65) % 26 + 65))] for letter in string.ascii_uppercase)
            if corr > max_corr:
                max_corr = corr
                max_corr_letter = chr(j + 65)
        key += max_corr_letter
    plaintext = ''
    j = 0
    for i in range(len(ciphertext)):
        if ciphertext[i].isalpha():
            ciphertext_letter = ciphertext[i]
            key_letter = key[j % key_length]
            plaintext_letter = chr((ord(ciphertext_letter) - ord(key_letter)) % 26 + 65)
            plaintext += plaintext_letter
            j += 1
        else:
            plaintext += ciphertext[i]

    return plaintext
